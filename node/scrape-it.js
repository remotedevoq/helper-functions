/*
This script uses scrape-it package to scrape a url and returns the result as a promise.
First, we get the list of urls from a txt file using the fs module.
Then we map through the array of urls to create a new array of promises returned from calling scrape on each url.
Then, using await and Promise.all, we put all the scraped data returned by scrape in a container object.
Finally, we write the data on a outout.json file using again the fs module.

*/

const scrape = require('scrape-it')
const fs = require('fs')

const urls = fs.readFileSync('links.txt').toString().split('\n')
const dataStructure = {
      title: '.headline__title',
      published_data: '.timestamp__date--published',
      imgurl: {
        selector: '.top-media--image .image__src',
        attr: 'src'
      },
      content: '.entry__text p'
    }
let posts = {}

const processArray = async (array) => {
  const promises = array.map( x => {
    return scrape(x, dataStructure).then(({data}) => data)
  })
  posts.posts = await Promise.all(promises)
  fs.writeFile('output.json', JSON.stringify(posts, null, 4), function(err) {
    console.log('File successfully written!')
  })
}

processArray(urls)