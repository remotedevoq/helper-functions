/*
When scrapping with Node.js, 3 mejor packages are used:
express, request and cheerio

With express with setup the server, with request we do the http request inside express and with cheerio we can extract content, text and data from the html received with request.

Cheerio is used just as jquery. In particular, when extracting data the .filter method is useful. It allows us to manipulate data with the filter callback. We use this when we need to assign the values an external variable and/or manipulate them before this. And if we don’t return anything inside the .filter callback, nothing happens.

Also, in nodejs, if a variable is assigned something without having previously declared it, the variable scopes up to become a global variable.
*/

const express = require('express')
const fs = require('fs')
const request = require('request')
const cheerio = require('cheerio')

let urls
let posts = { posts: [] }

let app = express()

app.get('/scrape', function(req, res) {
  // the url
  urls = fs.readFileSync('links.txt').toString().split('\n')
  for( let url of urls ) {
    request(url, function(error, response, html) {
      if(!error) {
        let $ = cheerio.load(html)
        let json = { title: "", published_data: "", imgurl: "", content: ""}

        json.title = $('.headline__title').text()
        json.published_data = $('.timestamp__date--published').text()
        json.imgurl = $('.top-media--image .image__src').attr('src')
        $('.entry__text').filter(function() {
          let entryText = $(this)
          entryText.find('p').each(function(i, el) {
            json.content += $(this).html()
          })
        })
        posts.posts.push(json)

        // $('.title_wrapper').filter(function() {
        //   let data = $(this)
        //   title = data.children().first().text()
        //   release = data.children().last().children().last().text()
        //   json.title = title
        //   json.release = release.replace(/\r?\n|\r/g, "")
        // })
        // $('.ratingValue strong span').filter(function() {
        //   let data = $(this)
        //   rating = data.text()
        //   json.rating = rating
        // })
        // console.log(json)
      }

      fs.writeFile('output.json', JSON.stringify(posts, null, 4), function(err) {
        console.log('File successfully written!')
      })
      res.send('check the server\'s console')
    })
  }
})

app.listen('8081')
console.log('Magic happening in port 8081')

exports = module.exports = app