<?php
// Get post's thumbnail url
$image = wp_get_attachment_image_src(
  get_post_thumbnail_id( $post->ID ),
  'single-post-thumbnail'
);