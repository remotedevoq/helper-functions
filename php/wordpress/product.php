<?php
// WooCommerce
// Get product's thumbnail url
$product_thumbnail_url = get_the_post_thumbnail_url( $product_object->get_id(), 'medium' );