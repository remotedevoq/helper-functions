#!/bin/bash

# this is just a script that contains all the instructions I had to manually type to get ghost installed.

# user creation
adduser dev
usermod -aG sudo dev
sudo cp ~/.ssh/authorized_keys /home/dev
su - dev

# ssh key set up
mkdir .ssh
sudo chown dev:dev ~./authorized_keys
sudo chmod 600 ~./authorized_keys
sudo mv ~/authorized_keys ~/.ssh/

# firewall set up
sudo ufw allow OpenSSH
sudo ufw enable

# installing packages
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install nginx
sudo ufw allow 'Nginx Full'
sudo apt-get install mysql-server
# set up root password. Write it down.
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash
sudo apt-get install -y nodejs
sudo npm i -g ghost-cli
sudo chown -R dev:dev /home/dev/.config

# set up ghost
sudo mkdir -p /var/www/ghost
sudo chown dev:dev ~/.ghost/config
sudo chown dev:dev /var/www/ghost
sudo chmod 775 /var/www/ghost
cd /var/www/ghost
ghost install