#!/bin/bash

rsync -azPv local/ root@200.200.200.200:/remote

# important to notice the trailing slash in the source path and no trailing slash in the destination path
# -a: archive, kinda like recursive
# -z: add compression
# -n: dry run
# -P: combines --progress to how each file goes and --partial to allow resuming of interrupted transfers.
# -v: verbose
# in newer version the --info=progress2 flag can be used to get progress of the whole operation
