#!/bin/bash

# to move everything to a folder without getting an error.
# make sure globbing wildcards are enabled.
setopt extended_glob #this is exclusive to zsh
mkdir target_folder # create the folder
mv -- *~target_folder target_folder
