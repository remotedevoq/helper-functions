# how to generate with openssl a key to generate a Root SSL certificate
# be ready to type a passphrase
openssl genrsa -des3 -out rootCA.key 2048

# and how to generate a certificate from that key
# where -days is the time of validity of the certificate
openssl req -x509 -new -nodes -key rootCA.key -sha256 -days 1024 -out rootCA.pem

# for more info go to:
https://medium.freecodecamp.org/how-to-get-https-working-on-your-local-development-environment-in-5-minutes-7af615770eec
