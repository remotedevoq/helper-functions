#!/bin/bash

find . -name "node_modules" -exec rm -rf '{}' +

# a more complete one is:
find . -name "node_modules" -type d -prune -exec rm -rf '{}' +
