#!bin/bash
# how to convert from png to jpg with imagemagick
convert rose.jpg -resize 50% rose.png

# convert webp to jpg
dwebp rose.webp -o - | convert - rose.png