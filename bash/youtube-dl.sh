#!/bin/bash

# just audio
youtube-dl -x --audio-format mp3 <video-url>

# best video quality
youtube-dl -f bestvideo+bestaudio <video-url>