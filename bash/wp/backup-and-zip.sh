#!/bin/bash

if type wp >/dev/null 2>&1; then
  wp db export --exclude_tables=wp_options,wp_users --porcelain | read DBEXNAME
  BKFOLDERNAME="backup-$(date +%s)"
  mkdir "${BKFOLDERNAME}"
  mv "${DBEXNAME}" "${BKFOLDERNAME}"
  if [ -d "./wp-content" ]; then
    cp -R wp-content "${BKFOLDERNAME}"
  fi
  zip -r "${BKFOLDERNAME}.zip" "${BKFOLDERNAME}"
else
  echo 'wp command does not exist'
fi