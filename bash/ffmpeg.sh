#!/bin/bash

# reduce the size of a file
ffmpeg -i hello-world_2.mp4 -vcodec libx264 -preset veryfast smaller.mp4
