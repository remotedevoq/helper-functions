#!/bin/bash

# zip a directory
zip -r myfiles.zip mydir

# unzip file into a directory
unzip file.zip -d destination_folder

# list the content of zip file
unzip -l archive.zip

# write the contents of named files to stdout without uncompressing
unzip -c archive.zip file1.txt file2.txt | less
