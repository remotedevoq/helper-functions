#!/bin/bash
# after installing tor and proxychains-ng with brew, you can run tor in a session and
# launch any app with it. In this case, Opera works fine.
# tho, remember to create the proxychains.conf file inside /etc/.

tor &
proxychains4 /Applications/Opera.app/Contents/MacOS/Opera

